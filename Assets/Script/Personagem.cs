﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using System.Threading;

public class Personagem : MonoBehaviour
{

    private int num;
    public Text TextColeta;

    public Image alma;
    public string item;

    public static bool pequeno;

    Transform rt;

    public int contador;

    public GameObject canva;

    //public GameObject fim;

    private float posicaoX;
    private float posicaoY;

    private Vector3 velocity = Vector3.zero;

    public float tempoAtivacao;

    // variáveis globais

    private Rigidbody2D myRigidbody;

    public TextMeshProUGUI vida;

    public int QntVida;
    public static bool estado;

    [Header("Movimento")]
    public bool olhandoDireita;
    public static float andar;

    public Animator animacao;

    public GameObject cam;

    [Header("Pulo")]
    private static bool podePular = false;

    public static float forcaPulo = 400f;

    public Transform posPe;
    public LayerMask layerChao;

    void Start()
    {
        alma.fillAmount = 0.02f;
        num = 0;
        pequeno = false;
        posicaoX = -2.617f;
        posicaoY = -7.350f;
        contador = 1;
        rt = GetComponent<Transform>();
        animacao = GetComponent<Animator>();
        vida.text = QntVida.ToString();
        forcaPulo = 100;
        myRigidbody = GetComponent<Rigidbody2D>();
    }


    void Update()
    {
        tempoAtivacao += Time.deltaTime;

        TextColeta.text = num.ToString() + "/2";

        vida.text = QntVida.ToString();


        if (NoChao() && Input.GetButtonDown("Jump"))
        {
            podePular = true;
        }

        direcao();
        movimentacao();
    }

    private void FixedUpdate()
    {
        if (podePular) Pular();
    }


    public void RecebeDano(int dano)
    {
        QntVida = Mathf.Clamp(QntVida - dano, 0, 3);
        if (QntVida <= 0)
        {
            GameOver.instance.fimdejogo();
        }
    }

    private void Pular()
    {
        podePular = false;
        myRigidbody.AddForce(Vector2.up * forcaPulo);

    }

    private bool NoChao()
    {
        return Physics2D.OverlapCircle(posPe.position, 0.02f, layerChao);
    }

    void movimentacao()
    {
        andar = Input.GetAxisRaw("Horizontal");

        myRigidbody.velocity = new Vector2(andar, myRigidbody.velocity.y);



        if (andar < 0)
        {
            olhandoDireita = true;
            animacao.SetBool("andando", true);

        }
        else if (andar > 0)
        {
            olhandoDireita = false;
            animacao.SetBool("andando", true);

        }
        else
        {
            animacao.SetBool("andando", false);
        }

    }

    void direcao()
    {

        if (olhandoDireita)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision2D)
    {
        

        if (collision2D.gameObject.CompareTag("parede") || /*collision2D.gameObject.CompareTag("obstaculo") ||*/ collision2D.gameObject.CompareTag("passagem"))
        {
            estado = false;
        }

        if (collision2D.gameObject.CompareTag(item))
        {
            num = num + 1;
            alma.fillAmount += 0.4f;
            Destroy(collision2D.gameObject);
        }

        if (collision2D.gameObject.CompareTag("passagem"))
        {
            if (pequeno)
            {
                Destroy(collision2D.gameObject);
            }
        }

        if (collision2D.gameObject.CompareTag("pequeno"))
        {
            Destroy(collision2D.gameObject);
            pequeno = !pequeno;

            if (pequeno)
            {
                transform.localPosition = new Vector3(-4.525f, -7.467f, 0.114f);

                contador = contador + 1;

                //CameraPlayer.frenteX -= -0.2f;
                animacao.SetBool("Dando", true);


                StartCoroutine(delay(1f));

                IEnumerator delay(float waitTime)
                {
                    yield return new WaitForSeconds(waitTime);

                    animacao.SetBool("Dando", false);
                    rt.localScale = new Vector3(0.4f, 0.4f, 0.4f);



                }
;

                
            }
            else
            {
                if (contador % 2 == 0)
                {
                    transform.localPosition = new Vector3(posicaoX, posicaoY, 0);
                    contador++;
                }

                //CameraPlayer.frenteX -= 0.2f;
                animacao.SetBool("Tirar", true);


                StartCoroutine(delay(1f));

                IEnumerator delay(float waitTime)
                {
                    yield return new WaitForSeconds(waitTime);


                    animacao.SetBool("Tirar", false);
                    rt.localScale = new Vector3(1f, 1f, 1f);


                }


            }
        }

        if (collision2D.gameObject.CompareTag("blockover"))
        {
            GetComponent<Personagem>().RecebeDano(3);
        }

    }
    private void OnCollisionExit2D(Collision2D collision2D)
    {
        if (collision2D.gameObject.CompareTag("parede") || /*collision2D.gameObject.CompareTag("obstaculo") ||*/ collision2D.gameObject.CompareTag("passagem"))
        {
            estado = true;
        }

    }

    public void Gravi(float num)
    {
        GetComponent<Rigidbody2D>().gravityScale = num;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("escada"))
        {
            GetComponent<Rigidbody2D>().gravityScale = 1;
        }
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("escada"))
        {
            GetComponent<Rigidbody2D>().gravityScale = 0;
            if (Input.GetKey("up"))
            {
                transform.position += Vector3.up / 60;
            }
            else if (Input.GetKey("down"))
            {
                transform.position += Vector3.down / 60;
            }
        }
    }
}


