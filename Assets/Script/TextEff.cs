﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextEff : MonoBehaviour
{
    public Image Txts, Txts2;
    public float FadeInTime = 2f;
    public bool FadeIn = true;

    void Start()
    {
        FadeIn = true;
        Txts.CrossFadeAlpha(0, 0f, true);
        Txts2.CrossFadeAlpha(0, 0f, true);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            Txts.CrossFadeAlpha(0, 0f, true);
            Txts2.CrossFadeAlpha(0, 0f, true);
        }

        if (FadeIn)
        {
            Txts.CrossFadeAlpha(1, FadeInTime, true);
            Txts2.CrossFadeAlpha(1, FadeInTime, true);
        }


    }
    
}