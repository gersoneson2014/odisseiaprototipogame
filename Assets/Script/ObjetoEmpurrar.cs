﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetoEmpurrar : MonoBehaviour
{
    public Transform ObjetoPonta;

    public LayerMask layerOBJ;

    public float tempo;

    public bool PodeCair;

    public bool estado = true;
    void Start()
    {
        tempo = 0;
        if (!estado)
        {
            PodeCair = false;
        }
        else { PodeCair = true; }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.G))
        {
            tempo += Time.deltaTime;
        }

        if (Input.GetKeyUp(KeyCode.G))
        {
            tempo = 0;
        }

        if(!estado)
        {
            if (tempo >= 2.5f)
            {
                PodeCair = true;
            }
            else { PodeCair = false; }
        }
        

        if (NoBloco() && PodeCair)
        {

            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            if (estado)
            {
                //GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
            }
            
        }
        else if(NoBloco() && tempo < 2.5f )
        {
            
              tempo = 0;
            //GetComponent<Animator>().SetBool("balancando", true);
        }else { GetComponent<Animator>().SetBool("balancando", false); }
    }

    public bool NoBloco()
    {
        return Physics2D.OverlapCircle(ObjetoPonta.position, 0.01f, layerOBJ);
    }

}
