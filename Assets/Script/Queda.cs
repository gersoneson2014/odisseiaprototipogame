﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Queda : MonoBehaviour
{

    public Transform ObjetoPonta;

    public LayerMask layerOBJ;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool NoBloco()
    {
        return Physics2D.OverlapCircle(ObjetoPonta.position, 0.01f, layerOBJ);
    }
}
