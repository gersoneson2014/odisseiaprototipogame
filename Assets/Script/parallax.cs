﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;

public class parallax : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject cameraPlayer;
    private float Length, startPos;
    public float speedParallax;

    void Start()
    {
        startPos = transform.position.x;
        Length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float tempo = (cameraPlayer.transform.position.x * (1 - speedParallax));
        float dist = (cameraPlayer.transform.position.x * speedParallax);
    
        transform.position = new Vector3(startPos + dist, transform.position.y, transform.position.z);
    
        /*if(tempo > startPos + Length)
        {
            startPos += Length;
        }else if(tempo < startPos - Length)
        {
            startPos -= Length;
        }*/
    }
}
