﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using JetBrains.Annotations;

public class Movimentacao : MonoBehaviour
{

    // variáveis globais

    private Rigidbody2D myRigidbody;


    public float velocidadeAndar;


    public float tempo;


    public static bool estado;

    public float tiltAroundY;

    [Header("Movimento")]
    public bool olhandoDireita; 

    public static float andar;

    public float QntVida;

    public Animator animacao;

    public bool NoPersonagem;

    public Transform ObjetoParaPe;

    [Header("Pulo")]
    private bool podePular = false;

    public float forcaPulo = 900f; 

    public Transform posPe; 

    public LayerMask layerChao;

    public bool estadoKeyG;

    public bool podeVirar; 

    public bool estadoChao;

    void Start()
    {
        ObjetoParaPe.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX;
        NoPersonagem = false;
        estado = true;
        estadoChao = true;
        podeVirar = true;
        estadoKeyG = false;
        andar = 0.3f;
        tiltAroundY = 0f;
        velocidadeAndar = 0.3f;
        animacao = GetComponent<Animator>();
        forcaPulo = 100;
        myRigidbody = GetComponent<Rigidbody2D>();
    }


    void Update()
    {
        if (Input.GetKey(KeyCode.G) && NoPersonagem)
        {
            ObjetoParaPe.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            ObjetoParaPe.position =  new Vector3(transform.position.x + 0.142781f, ObjetoParaPe.position.y, ObjetoParaPe.position.z);
        }
        else { ObjetoParaPe.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX; }

        if (estado)
        {
            Correr();
            movimentacao();
            direcao();
            Agarrar();

            if (NoChao() && Input.GetButtonDown("Jump"))
            {
                podePular = true;
            }
        }
        
    }

    private void FixedUpdate()
    {
        if (podePular) Pular();
    }

    private void Pular()
    {
        podePular = false;
        myRigidbody.AddForce(Vector2.up * forcaPulo);
    }

    public void Agarrar()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
                estadoKeyG = true;
                animacao.SetBool("empurrandoAndando", true);
                podeVirar = false;
        } 
        
        if (Input.GetKeyUp(KeyCode.G))
        {
            podeVirar = true;
            estadoKeyG = false;
            animacao.SetBool("empurrandoAndando", false);
        }
    }
    public void Correr()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            velocidadeAndar = 0.7f;
            animacao.SetBool("correndo", true);
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            velocidadeAndar = 0.3f;
            animacao.SetBool("correndo", false);
        }
    }

    public void RecebeDano(int dano)
    {
        QntVida = Mathf.Clamp(QntVida - dano,0,3);
        if (QntVida <= 0)
        {
            GameOver.instance.fimdejogo();
        }
    }
    
    private bool NoChao()
    {
        return Physics2D.OverlapCircle(posPe.position, 0.03f, layerChao); 
    }

    void movimentacao()
    {
        andar = Input.GetAxisRaw("Horizontal");

        Debug.Log(andar);

        myRigidbody.velocity = new Vector2(andar*velocidadeAndar, myRigidbody.velocity.y);

        if (andar < 0)
        {
            olhandoDireita = false;
            if (estadoKeyG == false)
            { animacao.SetBool("andando", true); }
        }
        else if (andar > 0)
        {
            olhandoDireita = true;
            if (estadoKeyG == false)
            { animacao.SetBool("andando", true); }
        }
        else 
        {
            animacao.SetBool("andando", false);
        }

    }

    void direcao()
    {

        Quaternion target = Quaternion.Euler(0, tiltAroundY, 0);

        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime*1000);

        if (podeVirar)
        {
            if (olhandoDireita)
            {
                tiltAroundY = 0;
            }
            else
            {
                tiltAroundY = 180;
            }
        }
    }


    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("parede"))
        {
            Debug.Log("Entrou Parede");
            NoPersonagem = true;
        }
        else { NoPersonagem = false; }
    }
    // Colisões com o personagem
    private void OnTriggerEnter2D(Collision2D collision2D)
    {
        if(collision2D.gameObject.CompareTag("Final"))
        {
            // Final

        }

        if (collision2D.gameObject.CompareTag("GameOver"))
        {
            // GameOver
        }
    }
      /*private void OnCollisionExit2D(Collision2D collision2D)
      {
      }*/
}
    

