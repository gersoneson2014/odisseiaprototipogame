﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;

public class CameraPlayer : MonoBehaviour
{

    public float timeY = 0.15f;

    public  float frenteX = -0.4f;

    public float cimaY = 0.5f;

    private Vector3 velocity = Vector3.zero;

    public Transform jogador;

    void Update()
    {
        Vector3 point = GetComponent<Camera>().WorldToViewportPoint(jogador.position);
        
        Vector3 delta = jogador.position - GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0, cimaY, point.z));
        
        Vector3 destinationY = transform.position + new Vector3(0, delta.y, 0);

        transform.position = Vector3.SmoothDamp(transform.position, destinationY, ref velocity, timeY);

        transform.localPosition = new Vector3(jogador.position.x - frenteX, transform.position.y,-1);

        Debug.Log(frenteX);
    }
}